@extends('layout.base')
@section('content')
    <div class="row">
        <div class="col">
            <p>Номер read-card</p>
            <h4 class="border p-2">{{auth_user()->read_card}}</h4>
            <p>Номер id-card</p>
            <h4 class="border p-2">{{auth_user()->id_card}}</h4>

            <h2>"Книга"{{$book->name}}</h2>
            <p>{{$book->author}}</p>
            <form action="{{route('auth.book.update', ['book'=>$book])}}" method="post">
                @csrf
                @method('PUT')
                <button type="submit" class="btn btn-primary">получить</button>
            </form>
        </div>
    </div>
@endsection
