@extends('layout.base')
@section('content')
    <div class="row">
        <a class="btn btn-sm btn-dark " style="width: 50px" href="{{route('home')}}">назад</a>
        <h4>мои книги</h4>
                @foreach($books as $book)
                    <div class="col-lg-4 mt-5">
                        <div class="card" style="width: 18rem;">
                            @if(is_null($book->picture))
                                <img class="card-img-top" src="{{asset('default.jpg')}}" alt="{{'default.jpg'}}">
                            @else
                                <img class="card-img-top" src="{{asset('/storage/' . $book->picture)}}" alt="{{$book->picture}}">
                            @endif
                            <div class="card-body">
                                <h6><i>Автор: </i>{{$book->author}}</h6>
                                <h6><i>Название: </i>{{$book->name}}</h6>
                            </div>
                                <form action="{{route('auth.users', ['book' => $book]) }}" method="post">
                                    @csrf
                                    @method('PUT')
                                    <button type="submit" class="btn btn-primary">Вернуть</button>
                                </form>

                        </div>
                    </div>
                @endforeach
            </div>
        </div>
    </div>

@endsection
