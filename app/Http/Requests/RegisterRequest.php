<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class RegisterRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     */
    public function authorize(): bool
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array<string, \Illuminate\Contracts\Validation\ValidationRule|array<mixed>|string>
     */
    public function rules(): array
    {
        return [
            'name' => ['required', 'min:2', 'max:255'],
            'lastname' => ['required', 'min:2', 'max:255'],
            'address' => ['required', 'min:2', 'max:255'],
            'id_card' => ['required', 'min:2', 'max:255', 'unique:users,id_card'],
            'read_card'=>['required', 'unique:users,read_card', 'max:255'],
            'password'=>['required', 'confirmed', 'min:6', 'max:255']
             ];
    }
}
