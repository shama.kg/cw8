<?php

namespace App\Http\Controllers;

use App\Http\Requests\LoginRequest;
use App\Models\User;
use Illuminate\Http\Request;

class SessionController extends AuthController
{
    public function loginForm(Request $request)
    {
        if ($request->session()->exists('read_card')){
            return redirect()->route('home')->with('error', 'You are already log in !');
        }
        return view('session.create');
    }

    public function store(LoginRequest $request)
    {
        $user = User::where('read_card', $request->get('read_card'))->first();

        if ($user){
            if ($this->auth($user, $request->get('password'))){
                $this->logIn($user);
                return redirect()->route('home')->with('success', 'Hello '. $user->name. '!');
            }
        }
        return redirect()->back()->with('error', 'Incorrect card_read or password');

    }
    public function destroy()
    {
        $this->logOut();
        return redirect()->route('auth.login-form')->with('success', 'You are success log out God Bye');
    }
}
