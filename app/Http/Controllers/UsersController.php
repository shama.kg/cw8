<?php

namespace App\Http\Controllers;

use App\Http\Requests\RegisterRequest;
use App\Models\Book;
use App\Models\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;

class UsersController extends AuthController
{
    public function registerForm(Request $request)
    {
        if ($request->session()->exists('read_card')){
            return redirect()->route('home')->with('error', 'You are already register!');
        }
        return view('users.register');
    }

    public function store(RegisterRequest $request)
    {
        $validated = $request->validated();
        $validated['read_card'] =$validated['read_card'].substr(md5(time()), 0, 16);
        $validated['password'] = Hash::make($validated['password']);
        $user = User::create($validated);

        $this->logIn($user);

        return redirect()->route('home')->with('success', 'Registration successfully, your read-card - '. $user->read_card);
    }

    public function index()
    {
        if (auth_user()){
            $books = Book::where('user_id', auth_user()->id)->get();
            return view('users.index', compact('books'));
        }else{
            return redirect()->route('auth.login');

        }
    }
    public function update(Request $request, Book $book)
    {
        $book->status = 0;
        $book->user_id = null;
        $book->update();
        return redirect()->route('category.index')->with('status', "Успешно отдан");

    }

}
