@extends('layout.base')

@section('content')
    <div class="row">
        <div class="col-6 offset-3 align-self-center">
            @include('notifications.alerts')
            <h1 class="text-center">Register</h1>
            <hr>
            <form action="{{route('auth.register')}}" method="post">
                @csrf
                <div class="form-group">
                    <label for="exampleInputName1">Name</label>
                    <input type="text" class="form-control" id="exampleInputName1" name="name">
                </div>
                <div class="form-group">
                    <label for="exampleInputName2">LastName</label>
                    <input type="text" class="form-control" id="exampleInputName2" name="lastname">
                </div>
                <div class="form-group">
                    <label for="exampleInputName2">Id-card</label>
                    <input type="text" class="form-control" id="exampleInputName2" name="id_card">
                </div>
                <div class="form-group">
{{--                    <label for="exampleInputName2">Ireadd-card</label>--}}
                    <input type="hidden" value="read" class="form-control" id="exampleInputName2" name="read_card">
                </div>
                <div class="form-group">
                    <label for="exampleInputName2">address</label>
                    <input type="text" class="form-control" id="exampleInputName2" name="address">
                </div>

                <div class="form-group">
                    <label for="exampleInputPassword1">Password</label>
                    <input type="password" class="form-control" id="exampleInputPassword1" name="password">
                </div>
                <div class="form-group">
                    <label for="exampleInputPassword2">Confirm password</label>
                    <input type="password" class="form-control" id="exampleInputPassword2" name="password_confirmation">
                </div>
                <button type="submit" class="btn btn-outline-primary">Получить read-card</button>
            </form>
        </div>
    </div>
@endsection
