<?php

namespace App\Http\Controllers;

use App\Models\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;


abstract class AuthController extends Controller
{
    protected function auth(User $user, string $password): bool
    {
        return Hash::check($password, $user->password);
    }

    protected function logIn(User $user): void
    {
        session()->put('read_card', $user->read_card);
    }
    protected function logOut(): void
    {
        session()->remove('read_card');
        session()->regenerate();
    }
}
