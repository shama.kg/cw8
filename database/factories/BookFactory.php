<?php

namespace Database\Factories;

use Illuminate\Database\Eloquent\Factories\Factory;
use Illuminate\Support\Facades\Storage;
use Intervention\Image\Facades\Image;

/**
 * @extends \Illuminate\Database\Eloquent\Factories\Factory<\App\Models\Book>
 */
class BookFactory extends Factory
{
    /**
     * Define the model's default state.
     *
     * @return array<string, mixed>
     */
    public function definition(): array
    {
        return [
            'name'=> $this->faker->userName,
            'picture' => $this->getImage(rand(1,4)),
            'author' => $this->faker->lastName,
            'category_id'=>rand(1,5),
            'user_id'=>rand(1,5),
            'status'=>rand(0,1),

        ];
    }
    private function getImage($image_number = 1): string
    {
        $path = storage_path() . "/seed_pictures/" . $image_number . ".jpeg";
        $image_name = md5($path) . '.jpeg';
        $resize = Image::make($path)->fit(300)->encode('jpeg');
        Storage::disk('public')->put('pictures/'.$image_name, $resize->__toString());
        return 'pictures/'.$image_name;

    }

}
