<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <title> Book store</title>

    <!-- Fonts -->
    <link href="https://fonts.googleapis.com/css?family=Nunito:200,600" rel="stylesheet">
    @vite(['resources/js/app.js'])

</head>
<body>
@include('notifications.alerts')
<div class="div d-flex justify-content-end" >
    @if(auth_user())
        <h6 class="p-4">Hello, {{auth_user()->name}} </h6>
        <form action="{{route('auth.logout')}}" method="post" class="p-2">
            @csrf
            @method('delete')
            <button type="submit" class="btn btn-sm btn-outline-danger">LOGOUT</button>
        </form>
    @else
        <a class="m-2 btn btn-outline-info" href="{{route('auth.register-form')}}">регистрация</a>
        <a class="m-2 btn btn-success" href="{{route('auth.login')}}">авторизация</a>
    @endif
</div>
<div class="container" >

    <h1>Книжная библиотека"</h1>
    @yield('content')

</div>
</body>
</html>
