<?php

use App\Http\Controllers\BookController;
use App\Http\Controllers\SessionController;
use App\Http\Controllers\UsersController;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider and all of them will
| be assigned to the "web" middleware group. Make something great!
|
*/
Route::get('/', [\App\Http\Controllers\CategoryController::class, 'index'])->name('home');


Route::resource('category', \App\Http\Controllers\CategoryController::class);

Route::prefix('auth')->name('auth.')->group(function (){
    Route::get('register', [UsersController::class, 'registerForm'])->name('register-form');
    Route::post('register', [UsersController::class, 'store'])->name('register');

    Route::get('login', [SessionController::class, 'loginForm'])->name('login-form');
    Route::post('login', [SessionController::class, 'store'])->name('login');
    Route::delete('log-out', [SessionController::class, 'destroy'])->name('logout');
    Route::get('my-book', [UsersController::class, 'index'])->name('mybook');
    Route::put('users/', [UsersController::class, 'update'])->name('users');
    Route::get('book/{$book}', [BookController::class, 'edit'])->name('edit');

    Route::resource('book', \App\Http\Controllers\BookController::class);

});
