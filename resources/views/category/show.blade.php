@extends('layout.base')
@section('content')
    <div class="row">
        <h4>Жанры</h4>
        <div class="col-lg-3">
            <ul>
                <li>
                    <a class="btn btn-outline-info" href="{{route('category.index')}}">Все</a>
                </li>
                @foreach($categories as $category)
                    <li>
                        <a class="btn btn-outline-info" href="{{route('category.show', ['category' => $category])}}">{{$category->name}}</a>

                    </li>
                @endforeach
            </ul>

        </div>
        <div class="col-lg-9">
            <div class="row">
                @foreach($books as $book)
                    <div class="col-lg-4 mt-5">
                        <div class="card" style="width: 18rem;">
                            @if(is_null($book->picture))
                                <img class="card-img-top" src="{{asset('default.jpg')}}" alt="{{'default.jpg'}}">
                            @else
                                <img class="card-img-top" src="{{asset('/storage/' . $book->picture)}}" alt="{{$book->picture}}">
                            @endif
                            <div class="card-body">
                                <h6><i>Автор: </i>{{$book->author}}</h6>
                                <h6><i>Название: </i>{{$book->name}}</h6>
                            </div>
                                @if($book->status)
                                    <ul>
                                        <li>Ожидается</li>
                                    </ul>
                                @else
                                    <ul class="list-group list-group-flush">
                                        <a href="{{route('auth.book.edit',['book' => $book])}}" class="btn btn-outline-primary">Получить</a>
                                    </ul>

                                @endif
                        </div>
                    </div>
                @endforeach
            </div>
        </div>
    </div>

@endsection
