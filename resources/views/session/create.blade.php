@extends('layout.base')

@section('content')
    <div class="row">
        <div class="col-6 offset-3 align-self-center">
            @include('notifications.alerts')
            <h1 class="text-center">Log in</h1>
            <hr>
            <form action="{{route('auth.login')}}" method="post">
                @csrf
                <div class="form-group">
                    <label for="exampleInputEmail1">Card-read</label>
                    <input type="text" class="form-control" id="exampleInputEmail1" aria-describedby="emailHelp" name="read_card">
                </div>
                <div class="form-group">
                    <label for="exampleInputPassword1">Password</label>
                    <input type="password" class="form-control" id="exampleInputPassword1" name="password">
                </div>
                <button type="submit" class="btn btn-primary">Submit</button> OR  <a href="{{route('auth.register')}}">register</a>
            </form>
        </div>
    </div>
@endsection
