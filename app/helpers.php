<?php

use App\Models\User;

function auth_user()
{
    if (session()->exists('read_card')){
        return User::where('read_card' , session('read_card'))->get()->first();
    }
    return null;
}
