<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class HomeController extends Controller
{
    public function index(Request $request)
    {
        if ($request->session()->exists('read_card')){
            return redirect()->route('home-page');
        }
            return view('home');
//        var_dump(session('user_id'));
    }
}
